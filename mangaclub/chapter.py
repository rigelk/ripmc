# coding: utf-8
"""Chapter-level module for Mangaclub

This module deals with downloading individual chapters.
"""

import os
import pathlib
from multiprocessing import Pool
from tqdm import tqdm
import requests

from .image import fix_image

#pylint: disable-msg=too-many-arguments
def get_all_from_chapter(url: str,
                         driver,
                         directory,
                         use_title_as_destination=False,
                         disable_progress_bar=False,
                         quit_after=True):
    """Gets chapter from the chapter url and downloads its images to a given directory

    Arguments:
        url (str): url to the chapter.
        driver: webdriver instance with security disabled to allow toDataURL method on canvas
        directory: destination where ./[Chapter name] will be placed.
        use_title_as_destination (bool, optional): title detected automatically
            will be used as title in place of the directory. Defaults to False.
        disable_progress_bar (bool, optional): disable the progress bar display. Defaults to False.
        quit_after (bool, optional): close the webdriver after all operations. Defaults to True.
    """
    driver.get(url)

    # extract data from the page
    title = driver.find_element_by_css_selector(".title-display > p:nth-child(1)").text
    pages = getPagesAsDict(driver.execute_script("return _pages")) # we have the 15 first pages
    total_pages = driver.execute_script("return Object.keys(PAGEMAP).length")
    current_page = len(pages)

    def _get_pages_around(number: int):
        driver.execute_script(
            "ajaxPageInfo(" + str(number) + ", function(number, _res, status){ "
            "_pages = JSON.parse(base64_decode(_res)).pages })"
        )
        _pages = driver.execute_script("return _pages")
        return getPagesAsDict(_pages) if pages else {}

    next_page_asked = min(total_pages, current_page + 7)
    pages = {**pages, **_get_pages_around(next_page_asked)}
    remaining_pages = total_pages - current_page
    while remaining_pages != 0:
        pages = {**pages, **_get_pages_around(next_page_asked)}
        current_page = len(pages)
        next_page_asked = min(total_pages, current_page)
        remaining_pages = total_pages - current_page

    # creating destination folder and naming it, if appliable
    folder = pathlib.Path(directory, title) if use_title_as_destination else directory
    if folder:
        pathlib.Path(folder).mkdir(parents=True, exist_ok=True)

    # build requests based on the data extracted from the page
    pages_url = [
        (pathlib.Path(folder, "{:04d}.jpg".format(page_number)), page_values[0], page_values[1])
        for page_number, page_values in pages.items()
    ]

    with Pool(10) as pool:
        list(tqdm(
            pool.imap_unordered(fetch_url, pages_url), total=len(pages_url),
            desc="Downloading chapter {}".format(folder),
            unit='image', unit_scale=True,
            disable=disable_progress_bar
        ))

    if quit_after:
        driver.close()

def fetch_url(entry):
    path, uri, shuffle_map = entry
    if not os.path.exists(path):
        req = requests.get(uri, stream=True)
        if req.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in req:
                    f.write(chunk)
    fix_image(path, shuffle_map)
    return path

def getPagesAsDict(pages: list) -> dict:
    return {
        page['page_number']: (page['page_url'], page['shuffle_map'])
        for page in pages
    }
