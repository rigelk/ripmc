# coding: utf-8
"""Image manipulation module for Mangaclub

Since manga.club hosts scrambled images, we have to re-assemble them.
The original algorithm from the reader is more or less as follows:

    ```js
    width = this.width;
    height = this.height;
    x_zoom = $(window).width() / (width * num_of_pages);
    y_zoom = $(window).height() / height;
    ratio = x_zoom / y_zoom;
    relatedWidth = window.innerHeight * (width / height);
    relatedHeight = window.innerWidth * (height / width) / num_of_pages;
    canvas.width = width; // resize
    canvas.height = height; // resize
    $(canvas).css('width', '100%');

    x_split_count = Math.floor(width / blocklen);
    y_split_count = Math.floor(height / blocklen);

    count = 0;

    ctx = setupCanvas(canvas, width, height) || createCanvas(i, width, height);
    if (!is_ios12) {
      ctx.drawImage(this, 0, 0, width, height);
    } else {
      ctx.drawImage(this, 0, y_split_count * blocklen + 1, width, height - y_split_count * BLOCKLEN, 0, y_split_count * BLOCKLEN, width, height - y_split_count * BLOCKLEN);
    }

    for (var i = 0; i < x_split_count; i++) {
      for (var j = 0; j < y_split_count; j++) {
        _x = _map[count][0];
        _y = _map[count][1];
        w = blocklen;
        h = blocklen;
        x = i * blocklen;
        y = j * blocklen;
        _w = blocklen;
        _h = blocklen;
        ctx.drawImage(this, x, y, w, h, _x, _y, _w, _h);
        count += 1;
      }
    }
    ```

It relies on a 'blocklen' variable, set to 128 most of the time, and a _map (otherwise called shuffle map), i.e.:

    ```
    {
      "page_number": 1,
      "page_url": "https://d205c5ezx6yg6v.cloudfront.net/Bikings_English/BIKINGS-en/001/001-3f3e3e40-b596-11e8-9e30-305a3a4b86b8.jpg",
      "shuffle_map": "[[0, 256, 128, 384], [512, 384, 640, 512], [640, 768, 768, 896], [640, 384, 768, 512], [512, 640, 640, 768], [128, 896, 256, 1024], [512, 1024, 640, 1152], [640, 0, 768, 128], [640, 896, 768, 1024], [384, 0, 512, 128], [128, 512, 256, 640], [512, 512, 640, 640], [384, 256, 512, 384], [0, 640, 128, 768], [256, 128, 384, 256], [512, 128, 640, 256], [512, 256, 640, 384], [0, 768, 128, 896], [512, 0, 640, 128], [640, 1024, 768, 1152], [256, 256, 384, 384], [0, 128, 128, 256], [640, 640, 768, 768], [0, 0, 128, 128], [384, 768, 512, 896], [256, 512, 384, 640], [256, 640, 384, 768], [0, 512, 128, 640], [256, 1024, 384, 1152], [256, 768, 384, 896], [128, 768, 256, 896], [384, 128, 512, 256], [256, 384, 384, 512], [128, 256, 256, 384], [384, 384, 512, 512], [384, 512, 512, 640], [640, 256, 768, 384], [256, 0, 384, 128], [0, 384, 128, 512], [640, 512, 768, 640], [512, 768, 640, 896], [128, 128, 256, 256], [384, 640, 512, 768], [128, 1024, 256, 1152], [128, 640, 256, 768], [640, 128, 768, 256], [0, 896, 128, 1024], [384, 1024, 512, 1152], [384, 896, 512, 1024], [256, 896, 384, 1024], [0, 1024, 128, 1152], [512, 896, 640, 1024], [128, 0, 256, 128], [128, 384, 256, 512]]"
    }
    ```

More info at https://stackoverflow.com/a/14594003

"""

import math
from pathlib import Path
import json
from PIL import Image

def fix_image(path: Path, shuffle_map: str):
    """Creating unscrambled image from source"""
    # setup from source
    img = Image.open(path)
    source_width, source_height = img.size
    blank_image = Image.new(img.mode, (source_width, source_height), 'white')
    shuffle_map = json.loads(shuffle_map)

    # manipulation
    blocklen = 128
    x_split_count = math.floor(source_width / blocklen)
    y_split_count = math.floor(source_height / blocklen)
    count = 0

    for i in range(x_split_count):
        for j in range(y_split_count):
            _x = shuffle_map[count][0]
            _y = shuffle_map[count][1]
            w = blocklen
            h = blocklen
            x = i * blocklen
            y = j * blocklen

            crop = img.crop((x, y, x+w, y+h))
            blank_image.paste(crop, (_x, _y))

            count += 1

    # save fixed image
    blank_image.save(path)
