# -*- coding: utf-8 -*-
"""Mangaclub module

This module defines functions to rip mangas and their chapters from manga.club.

Todo:
    * Define a class implementation so that it can share a common interface with
      other site rippers
    * Parallelise ripping chapters
"""

from .chapter import get_all_from_chapter
from .manga import get_all_chapters_from_page
from .webdriver_utils import install_browsermobproxy

__all__ = [
    "get_all_from_chapter",
    "get_all_chapters_from_page",
    "install_browsermobproxy"
]
