# coding: utf-8
"""Manga-level module for Mangaclub

This module deals with general information extraction from mangas.
"""

import pathlib

from .chapter import get_all_from_chapter

def get_all_chapters_from_page(url: str, driver, directory, overwrite=False):
    """Gets all chapters from the manga url and downloads them to a given directory

    Args:
        url (str): url to the manga
        driver: webdriver instance with security disabled to allow toDataURL method on canvas
        directory: destination where ./[Manga name]/[Chapter name] will be placed
    """
    driver.get(url)

    # main selectors
    # title = driver.find_element_by_css_selector("h1.translated").text
    chapter_list = driver.find_elements_by_css_selector(
        "div.my-3 > div:nth-child(2) > div > div:nth-child(1) > a:nth-child(1)"
    )

    # building list of chapters to download (some are probably for logged-in users)
    chapter_list_to_download = []
    chapter_skipped_counter = 0
    for i in chapter_list:
        chapter_title = i.text.replace('Free ', '')
        chapter_url = i.get_attribute("href")
        if "login-required-story" not in i.get_attribute("class"):
            chapter_list_to_download.append(tuple((chapter_title, chapter_url)))
        else:
            chapter_skipped_counter += 1

    print("{} chapter(s) will now be downloaded out of {} available on the platform."
          .format(len(chapter_list)-chapter_skipped_counter, len(chapter_list)))

    for chapter_title, chapter_url in chapter_list_to_download:
        if pathlib.Path(directory, chapter_title).exists() and not overwrite:
            print("Skipping {} as it has already been downloaded.".format(chapter_title))
            continue
        get_all_from_chapter(chapter_url, driver,
                             pathlib.Path(directory, chapter_title),
                             quit_after=False)

    driver.close()
