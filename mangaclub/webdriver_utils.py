# coding: utf-8
"""Utilities to configure the Selenium webdriver"""

import zipfile
import pathlib
import os
import stat
from io import BytesIO
import requests

def make_executable(path):
    mode = os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(path, mode)

def install_browsermobproxy():
    """Installs browsermob-proxy to vender/browsermobproxy"""
    if pathlib.Path('vendor/browsermobproxy').exists():
        return

    request = requests.get('https://github.com/lightbody/browsermob-proxy/releases/download/browsermob-proxy-2.1.4/browsermob-proxy-2.1.4-bin.zip')
    zip_document = zipfile.ZipFile(BytesIO(request.content))
    zip_document.extractall('vendor/browsermobproxy')

    make_executable('vendor/browsermobproxy/browsermob-proxy-2.1.4/bin/browsermob-proxy')
