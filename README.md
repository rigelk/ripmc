# rip.manga.club

As its name implies, the script downloads mangas from manga.club.

## Installation

```bash
git clone https://framagit.org/rigelk/ripmc.git
# install poetry, the python dependency manager used to wrap things up
curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
```

While within the cloned directory (`cd ripmc`), you can install the dependencies required to run the script.

```bash
# install the dependencies
poetry install
```

## Usage

You can alias the script to the python runtime of a virtualenv with the dependencies, or prefix the script calls by `poetry run`.

```bash
ripmangaclub --help
ripmangaclub --chapter=https://www.manga.club/bv/t/BIKINGS-en/v/1/s/1/p/1
ripmangaclub --manga=https://www.manga.club/book/title/BIKINGS-en/
```
